package info.airdrop.airdropweb.entity;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

public class News implements DomainObject {
    private UUID id;
    private String title;
    private String text;
    private String shortText;
    private Boolean top;
    private Timestamp creationDate;
    private Timestamp updateDate;

    public News(UUID id, String title, String text, String shortText, Boolean top, Timestamp creationDate, Timestamp updateDate) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.shortText = shortText;
        this.top = top;
        this.creationDate = creationDate;
        this.updateDate = updateDate;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public Boolean getTop() {
        return top;
    }

    public void setTop(Boolean top) {
        this.top = top;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        News news = (News) o;
        return Objects.equals(id, news.id) &&
                Objects.equals(title, news.title) &&
                Objects.equals(text, news.text) &&
                Objects.equals(shortText, news.shortText) &&
                Objects.equals(top, news.top) &&
                Objects.equals(creationDate, news.creationDate) &&
                Objects.equals(updateDate, news.updateDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, text, shortText, top, creationDate, updateDate);
    }
}

package info.airdrop.airdropweb.entity;

import java.util.Objects;
import java.util.UUID;

public class NewsType implements DomainObject {
    private UUID id;
    private String description;

    public NewsType(UUID id, String description) {
        this.id = id;
        this.description = description;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NewsType newsType = (NewsType) o;
        return Objects.equals(id, newsType.id) &&
                Objects.equals(description, newsType.description);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, description);
    }
}

package info.airdrop.airdropweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AirdropWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(AirdropWebApplication.class, args);
	}
}

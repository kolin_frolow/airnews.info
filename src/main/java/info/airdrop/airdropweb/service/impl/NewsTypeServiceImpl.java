package info.airdrop.airdropweb.service.impl;

import info.airdrop.airdropweb.entity.NewsType;
import info.airdrop.airdropweb.repository.DataRepository;
import info.airdrop.airdropweb.repository.NewsTypeRepository;
import info.airdrop.airdropweb.service.NewsTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service("newsTypeService")
public class NewsTypeServiceImpl implements NewsTypeService {

    private static final Logger LOG = LoggerFactory.getLogger(NewsTypeServiceImpl.class);

    @Autowired
    @Qualifier("newsTypeRepository")
    private NewsTypeRepository repository;

    @Override
    public Set<NewsType> getAllTypes() {
        return repository.getAllTypes();
    }
}

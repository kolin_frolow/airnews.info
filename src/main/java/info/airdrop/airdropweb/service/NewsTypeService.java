package info.airdrop.airdropweb.service;

import info.airdrop.airdropweb.entity.NewsType;

import java.util.Set;

public interface NewsTypeService {
    public Set<NewsType> getAllTypes();
}

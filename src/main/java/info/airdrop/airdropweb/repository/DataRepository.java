package info.airdrop.airdropweb.repository;

import info.airdrop.airdropweb.entity.DomainObject;

import java.util.Set;
import java.util.UUID;

public interface DataRepository<V extends DomainObject> {
    void persist(V object);

    void delete(V object);

    void edit(V object);

    V find(UUID id);

    Set<V> findData(int count, int page);

    Set<V> getAllData();
}

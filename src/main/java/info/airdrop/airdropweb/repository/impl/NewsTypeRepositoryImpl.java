package info.airdrop.airdropweb.repository.impl;

import info.airdrop.airdropweb.entity.NewsType;
import info.airdrop.airdropweb.repository.DataRepository;
import info.airdrop.airdropweb.repository.NewsTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import java.sql.Types;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@org.springframework.stereotype.Repository("newsTypeRepository")
public class NewsTypeRepositoryImpl implements NewsTypeRepository {
    @Autowired
    protected JdbcOperations jdbcOperations;

    @Override
    public void persist(NewsType object) {
        Object[] params = new Object[] { UUID.fromString(object.getDescription()), object.getDescription() };
        int[] types = new int[] { Types.VARCHAR, Types.VARCHAR };

        jdbcOperations.update("INSERT INTO news(\n" +
                "            id, description)\n" +
                "    VALUES (cast(? as UUID), ?);", params, types);
    }

    @Override
    public void delete(NewsType object) {
        //
    }

    @Override
    public void edit(NewsType object) {

    }

    @Override
    public NewsType find(UUID id) {
        return null;
    }

    @Override
    public NewsType find(String description) {
        return null;
    }

    @Override
    public Set<NewsType> getAllTypes() {
        Set<NewsType> result = new HashSet<>();
        SqlRowSet rowSet = jdbcOperations.queryForRowSet("SELECT id,description FROM type p ORDER BY description;");
        while (rowSet.next()) {
            result.add(new NewsType(UUID.fromString(rowSet.getString("id")), rowSet.getString("description")));
        }
        return result;
    }
}

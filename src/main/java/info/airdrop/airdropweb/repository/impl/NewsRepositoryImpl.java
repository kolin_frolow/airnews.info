package info.airdrop.airdropweb.repository.impl;

import info.airdrop.airdropweb.entity.News;
import info.airdrop.airdropweb.repository.DataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;

import java.sql.Types;
import java.util.Set;
import java.util.UUID;

@org.springframework.stereotype.Repository("dataRespitory")
public class NewsRepositoryImpl implements DataRepository<News> {
    @Autowired
    protected JdbcOperations jdbcOperations;

    @Override
    public void persist(News object) {

        Object[] params = new Object[] { object.getId(), object.getTitle() };
        int[] types = new int[] { Types.VARCHAR, Types.VARCHAR };

        jdbcOperations.update("INSERT INTO news(\n" +
                "            data_id, data_description)\n" +
                "    VALUES (cast(? as UUID), ?);", params, types);
    }

    @Override
    public void delete(News object) {

    }

    @Override
    public void edit(News object) {

    }

    @Override
    public News find(UUID id) {
        return null;
    }

    @Override
    public Set<News> findData(int count, int page) {
        return null;
    }

    @Override
    public Set<News> getAllData() {
        return null;
    }
}

package info.airdrop.airdropweb.repository;

import info.airdrop.airdropweb.entity.NewsType;

import java.util.Set;
import java.util.UUID;

public interface NewsTypeRepository {
    void persist(NewsType object);

    void delete(NewsType object);

    void edit(NewsType object);

    NewsType find(UUID id);

    NewsType find(String description);

    Set<NewsType> getAllTypes();
}
